package com.kzabinski.junit_example;

public class ArithmeticOperations {

    public double divide(double a, double b) {
        return a / b;
    }

    public double multiply(double... input) {
        double result = 1;
        for (double value : input) {
            result *= value;
        }
        return result;
    }
}
