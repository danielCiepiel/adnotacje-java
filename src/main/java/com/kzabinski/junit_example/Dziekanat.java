package com.kzabinski.junit_example;
import java.util.Arrays;

public class Dziekanat {

    public String[] listaProwadzacych() {
        return new String[]{"Krzysztof Żabiński", "Robert Pietrzyk", "Mariusz Kalita"};
    }

    public String wprowadzOcene(double ocena) {
        if (ocena < 2.0) {
            return "Wprodzadzona ocena musi wieksza lub rowna 2";
        }
        if (ocena > 5.0) {
            return "Ocena nie moze byc wieksza nic 5.0";
        }
        return "Ocena wprowadzona: " + Double.toString(ocena);
    }

    public String znajdzProwadzacego(String szukanyProwadzacy) {

        if (szukanyProwadzacy == "") {
            return "Nie wprowadzono szukanego tekstu";
        }

        String[] listaProwadzacych = this.listaProwadzacych();

        int getIndex = Arrays.asList(this.listaProwadzacych()).indexOf(szukanyProwadzacy);

        if (getIndex > 0) {
            return listaProwadzacych[getIndex];
        } else {
            return "Nie znaleziono prowadzacego";
        }
    }
}
