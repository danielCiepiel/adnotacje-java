package com.kzabinski.junit_example;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
class JunitExampleApplicationTests {

    ArithmeticOperations arithmeticOperations = new ArithmeticOperations();
    Dziekanat dziekanat = new Dziekanat();

    @Test
    void testDziekanatuNieMaTakiejOceny() {
        Assert.isTrue(dziekanat.wprowadzOcene(6.0).contains("Ocena nie moze byc wieksza nic 5.0"), "test ocen wiekszych od 6");
    }

    @Test
    void testDziekanatuNieMoznaWprowadzicUjemnejOceny() {
        Assert.isTrue(dziekanat.wprowadzOcene(-3.0).contains("Wprodzadzona ocena musi wieksza lub rowna 2"), "test ocen ujemnych");
    }

    @Test
    void testDziekanatuZnajdzProwadzacego() {
        Assert.isTrue(dziekanat.znajdzProwadzacego("Mariusz Kalita").contains("Mariusz Kalita"), "prowadzacy odnaleziony");
    }

    @Test
    void testDziekanatuNieZnalezionoProwadzacego() {
        Assert.isTrue(dziekanat.znajdzProwadzacego("Andrzej Duda").contains("Nie znaleziono prowadzacego"), "prowadzacy nie odnaleziony");
    }

    @Test
    void testDziekanatuZnajdzProwadzacegoNieWprowadzonoDanych() {
        Assert.isTrue(dziekanat.znajdzProwadzacego("").contains("Nie wprowadzono szukanego tekstu"), "nie wprowadzono danych");
    }
}
